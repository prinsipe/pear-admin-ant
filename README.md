
<div align="center">
<br/>

  <h1 align="center">
    Pear Admin Ant
  </h1>
  <h4 align="center">
    开 箱 即 用 的 Vue 3.0 前 端 开 发 框 架
  </h4> 

  [预 览](http://ant.pearadmin.com)   |   [官 网](http://www.pearadmin.com/)   |   [群聊](https://jq.qq.com/?_wv=1027&k=5OdSmve)   |   [社区](http://forum.pearadmin.com/)

</div>

<p align="center">
    <a href="#">
        <img src="https://img.shields.io/badge/Pear Admin Ant-1.3.0.release-green.svg" alt="Pear Admin Ant Version">
    </a>
    <a href="#">
        <img src="https://img.shields.io/badge/Vue-3.0.0+-green.svg" alt="Vue Version">
    </a>
      <a href="#">
        <img src="https://img.shields.io/badge/Ant Design Vue-2.0.0.beta+-green.svg" alt="Ant Design Vue Version">
    </a>
</p>
<div align="center">
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="https://images.gitee.com/uploads/images/2020/1112/005939_5a252a51_4835367.png" />
</div>

### 开源地址

Gitee 开源地址: [前往下载](https://gitee.com/pear-admin/pear-admin-ant)


Github 开源地址: [前往下载](https://github.com/PearAdmin/Pear-Admin-Ant)

#### 项目结构

```
Pear Admin Ant
│
├─dist  打包目录
│
├─src 源码
│  │
│  |-api 网络请求
│  |
│  |-assets 静态资源
│  |
│  |-component 常用组件    
│  |   
│  |-config 配置文件
│  |
│  |-directives 指令目录
│  |
│  |-layout 布局组件
│  |
│  |-mock 模拟数据
│  |
│  |-router 路由管理
│  | 
│  |-utils 工具类
│  |
│  |-view 页面目录
│  |
│  |-App.vue 主体视图
│  │
│  └─main.js 入口文件
│  
└─pom.xml  Maven 配置

```


### 主题模式

| 默认主题  | 白色主题  |  夜间主题  |
|---|---|---|
| ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150340_f168fc5d_4835367.png "黑色主题.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150351_953099c2_4835367.png "白色主题.png")  |  ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150406_27a29e34_4835367.png "夜间主题.png")  |
|---|---|---|

### 布局方式

| 侧边布局  | 顶部布局 |  联动布局  |
|---|---|---|
| ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150535_ad4ad37a_4835367.png "黑色主题.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150548_2d6381ee_4835367.png "顶部布局.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/1108/150557_bc164bfc_4835367.png "联动布局.png")  |
|---|---|---|

### 主题配色

| 黄色主题  | 绿色主题 |  蓝色主题  |
|---|---|---|
| ![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/005917_e2433885_4835367.png "黄色.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/005939_5a252a51_4835367.png "绿色.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2020/1112/005952_3c465c30_4835367.png "蓝色.png") |
|---|---|---|

### 多选项卡

| DOT主题  | CARD主题 |  BTN主题  |
|---|---|---|
| ![输入图片说明](https://images.gitee.com/uploads/images/2021/0124/181653_e19b682a_4835367.png "选项卡1.png") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0124/181711_5af239fd_4835367.png "选项卡2.png")  | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0124/181730_1718d754_4835367.png "选项卡3.png") |
|---|---|---|


### 项目安装
```
npm install

或

cnpm install
```

### 项目运行
```
npm run serve-dev

或

npm run serve-prod
```

### 编译项目
```
npm run build-dev

或

npm run build-prod
```

## 示例截图

![输入图片说明](https://images.gitee.com/uploads/images/2020/1104/172456_91ab3275_4835367.png "ssssssssssssss.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1115/210101_56754a0e_4835367.png "主题.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214245_00adb66f_4835367.png "ant1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214254_4677557c_4835367.png "ant2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214302_b36f13d8_4835367.png "ant3.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214310_a363ecb3_4835367.png "ant4.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214319_5e551a1b_4835367.png "ant5.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214328_b50b62e8_4835367.png "ant6.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214337_e99d7b78_4835367.png "ant7.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214346_4e5261c2_4835367.png "ant8.png")

![输入图片说明](https://images.gitee.com/uploads/images/2020/1025/214354_d3c1d47d_4835367.png "ant9.png")
